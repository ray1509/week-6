posibilities = [
    ('scissors', 'cuts', 'paper'),
    ('paper', 'covers', 'rock'),
    ('rock', 'crushes', 'lizard'),
    ('lizard', 'poison', 'spock'),
    ( 'spock', 'smashes', 'scissors'),
    ( 'scissors', 'decapitate', 'lizard'),
    ( 'lizard', 'eats', 'paper'),
    ( 'paper', 'disproves', 'spock'),
    ( 'spock', 'vaporizes', 'rock'),
    ( 'rock', 'crushes', 'scissors'),
]

def game(player1_input, player2_input):

    for posibility in posibilities:
        if player1_input == None or player2_input == None:
           return None
        elif posibility[0] == player1_input and posibility[2] == player2_input:
            a = posibility[0] + ' '+ posibility[1] + ' ' + posibility[2]
            return a

assert game(None, None) == None, 'Expected value is `None`'
assert game('', None) == None, 'Expected value is `None`'
assert game('Tijera', 'PAper') == None, 'Expected value is `None`'
assert game('scissors', 'paper') == 'scissors cuts paper', 'Expected value is `scissors cuts paper`'
assert game('paper', 'rock') == 'paper covers rock', 'Expected value is `paper covers rock`'
assert game('rock', 'lizard') == 'rock crushes lizard', 'Expected value is `rock crushes lizard`'
assert game('lizard', 'spock') == 'lizard poison spock', 'Expected value is `lizard poison spock`'
assert game('spock', 'scissors') == 'spock smashes scissors', 'Expected value is `spock smashes scissors`'
assert game('scissors', 'lizard') == 'scissors decapitate lizard', 'Expected value is `scissors decapitate lizard`'
assert game('lizard', 'paper') == 'lizard eats paper', 'Expected value is `lizard eats paper`'
assert game('paper', 'spock') == 'paper disproves spock', 'Expected value is `paper disproves spock`'
assert game('spock', 'rock') == 'spock vaporizes rock', 'Expected value is `spock vaporizes rock`'
assert game('rock', 'scissors') == 'rock crushes scissors', 'Expected value is `rock crushes scissors`'