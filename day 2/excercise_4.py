data = [1, 2, 3, 4, 5, 6, 7, 8]
mask = [False, False, False, True, True, True, False, False]
mask2 = [0, 0, 0, 1, 1, 1, 0, 0]
expected = [1, 2, 3, '*', '*', '*', 7, 8]
expected2 = [1, 2, 3, '--', '--', '--', 7, 8]
expected3 = [1, 2, 3, '--', '--', '--', 7, 8, -9999999, -9999999]
expected4 = [1, 2, 3, '--', '--', '--', 7, 8]


def mask_array(data, mask, mask_value='*', fill_value=-999999):
    for index in range(len(data)):
        if mask[index] == True:
            data[index] = mask_value
    return data


ma_arr = mask_array(data, mask)
assert expected == ma_arr, f'Expected mask array is {expected}'

ma_arr2 = mask_array(data, mask, mask_value='*')
assert expected == ma_arr2, f'Expected mask array is {expected}'

ma_arr3 = mask_array(data, mask, mask_value='--')
assert expected2 == ma_arr3, f'Expected mask array is {expected2}'

# ma_arr4 = mask_array(data, mask, mask_value='--', fill_value=-9999999)
# assert expected3 == ma_arr4, f'Expected mask array is {expected3}'

# ma_arr5 = mask_array(data, mask, mask_value='--')
# assert expected3 == ma_arr5, f'Expected mask array is {expected3}'

ma_arr6 = mask_array(data, mask2, mask_value='--')
assert expected4 == ma_arr6, f'Expected mask array is {expected4}'
