numbers = [ 1 , 3 , 5 , 8 , 10 , 13 , 18 , 36 , 78 ]
expected_result = [10, 18, 78]


new_list = [numbers[position] for position in range(len(numbers)) if numbers[position]%2 ==0 and position%2 ==0]

print(new_list)

assert expected_result == new_list, f"Expected result is {expected_result}"