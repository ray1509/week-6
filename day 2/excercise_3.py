posibilities = {
    'cuts': ['scissors', 'paper'],
    'covers': ['paper', 'rock'],
    'crush': ['rock', 'lizard'],
    'poison': ['lizard', 'spock'],
    'smashes': ['spock', 'scissors'],
    'decapitate': ['scissors', 'lizard'],
    'eats': ['lizard', 'paper'],
    'disproves': ['paper', 'spock'],
    'vaporizes': ['spock', 'rock'],
    'crushes': ['rock', 'scissors'],
}


def game(player1_input, player2_input):

    if (player1_input in [None, '', {}]) or (player2_input in [None, '', {}]):
        return(None)
    else:
        for action, option in posibilities.items():
            if player1_input.get('option') == player2_input.get('option'):
                return ('Player1 and Player2 are even!')
            elif player1_input.get('option') == option[0] and player2_input.get('option') == option[1]:
                return (option[0] + ' ' + action + ' ' + option[1] + ', ' +
                      'Player' + str(player1_input.get('id')) + ' won!')


assert game(None, None) == None, 'Expected value is `None`'
assert game('', None) == None, 'Expected value is `None`'
assert game({}, {}) == None, 'Expected value is `None`'
assert game({'id': 1, 'option': 'Tijera'}, {'id': 2, 'option': 'PAper'}) == None, 'Expected value is `None`'
assert game({'id': 1, 'option': 'scissors'}, {'id': 2, 'option': 'scissors'}) == 'Player1 and Player2 are even!', 'Expected value is `Player1 and Player2 are even!`'
assert game({'id': 1, 'option': 'scissors'}, {'id': 2, 'option': 'paper'}) == 'scissors cuts paper, Player1 won!', 'Expected value is `scissors cuts paper, Player1 won!`'
assert game({'id': 1, 'option': 'paper'}, {'id': 2, 'option': 'rock'}) == 'paper covers rock, Player1 won!', 'Expected value is `paper covers rock, Player1 won!`'
assert game({'id': 1, 'option': 'rock'}, {'id': 2, 'option': 'lizard'}) == 'rock crush lizard, Player1 won!', 'Expected value is `rock crush lizard, Player1 won!`'
assert game({'id': 1, 'option': 'lizard'}, {'id': 2, 'option': 'spock'}) == 'lizard poison spock, Player1 won!', 'Expected value is `lizard poison spock, Player1 won!`'
assert game({'id': 1, 'option': 'spock'}, {'id': 2, 'option': 'scissors'}) == 'spock smashes scissors, Player1 won!', 'Expected value is `spock smashes scissors, Player1 won!`'
assert game({'id': 1, 'option': 'scissors'}, {'id': 2, 'option': 'lizard'}) == 'scissors decapitate lizard, Player1 won!', 'Expected value is `scissors decapitate lizard, Player1 won!`'
assert game({'id': 1, 'option': 'lizard'}, {'id': 2, 'option': 'paper'}) == 'lizard eats paper, Player1 won!', 'Expected value is `lizard eats paper, Player1 won!`'
assert game({'id': 1, 'option': 'paper'}, {'id': 2, 'option': 'spock'}) == 'paper disproves spock, Player1 won!', 'Expected value is `paper disproves spock, Player1 won!`'
assert game({'id': 1, 'option': 'spock'}, {'id': 2, 'option': 'rock'}) == 'spock vaporizes rock, Player1 won!', 'Expected value is `spock vaporizes rock, Player1 won!`'
assert game({'id': 1, 'option': 'rock'}, {'id': 2, 'option': 'scissors'}) == 'rock crushes scissors, Player1 won!', 'Expected value is `rock crushes scissors, Player1 won!`'

assert game({'id': 2, 'option': 'scissors'}, {'id': 1, 'option': 'paper'}) == 'scissors cuts paper, Player2 won!', 'Expected value is `scissors cuts paper, Player2 won!`'
assert game({'id': 2, 'option': 'paper'}, {'id': 1, 'option': 'rock'}) == 'paper covers rock, Player2 won!', 'Expected value is `paper covers rock, Player2 won!`'
assert game({'id': 2, 'option': 'rock'}, {'id': 1, 'option': 'lizard'}) == 'rock crush lizard, Player2 won!', 'Expected value is `rock crush lizard, Player2 won!`'
assert game({'id': 2, 'option': 'lizard'}, {'id': 1, 'option': 'spock'}) == 'lizard poison spock, Player2 won!', 'Expected value is `lizard poison spock, Player2 won!`'
assert game({'id': 2, 'option': 'spock'}, {'id': 1, 'option': 'scissors'}) == 'spock smashes scissors, Player2 won!', 'Expected value is `spock smashes scissors, Player2 won!`'
assert game({'id': 2, 'option': 'scissors'}, {'id': 1, 'option': 'lizard'}) == 'scissors decapitate lizard, Player2 won!', 'Expected value is `scissors decapitate lizard, Player2 won!`'
assert game({'id': 2, 'option': 'lizard'}, {'id': 1, 'option': 'paper'}) == 'lizard eats paper, Player2 won!', 'Expected value is `lizard eats paper, Player2 won!`'
assert game({'id': 2, 'option': 'paper'}, {'id': 1, 'option': 'spock'}) == 'paper disproves spock, Player2 won!', 'Expected value is `paper disproves spock, Player2 won!`'
assert game({'id': 2, 'option': 'spock'}, {'id': 1, 'option': 'rock'}) == 'spock vaporizes rock, Player2 won!', 'Expected value is `spock vaporizes rock, Player2 won!`'
assert game({'id': 2, 'option': 'rock'}, {'id': 1, 'option': 'scissors'}) == 'rock crushes scissors, Player2 won!', 'Expected value is `rock crushes scissors, Player2 won!`'
