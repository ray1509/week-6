posibilities = {
    'cuts': ['scissors', 'paper'],
    'covers': ['paper', 'rock'],
    'crush': ['rock', 'lizard'],
    'poison': ['lizard', 'spock'],
    'smashes': ['spock', 'scissors'],
    'decapitate': ['scissors', 'lizard'],
    'eats': ['lizard', 'paper'], 
    'disproves': ['paper', 'spock'],
    'vaporizes': ['spock', 'rock'], 
    'crushes': ['rock', 'scissors'], 
}

def game(player1_input, player2_input):
    for action, option  in posibilities.items():
        if player1_input == option[0] and player2_input == option[1]:
            return option[0] + ' ' + action + ' ' + option[1]

assert game(None, None) == None, 'Expected value is `None`'
assert game('', None) == None, 'Expected value is `None`'
assert game('Tijera', 'PAper') == None, 'Expected value is `None`'
assert game('scissors', 'paper') == 'scissors cuts paper', 'Expected value is `scissors cuts paper`'
assert game('paper', 'rock') == 'paper covers rock', 'Expected value is `paper covers rock`'
assert game('rock', 'lizard') == 'rock crush lizard', 'Expected value is `rock crush lizard`'
assert game('lizard', 'spock') == 'lizard poison spock', 'Expected value is `lizard poison spock`'
assert game('spock', 'scissors') == 'spock smashes scissors', 'Expected value is `spock smashes scissors`'
assert game('scissors', 'lizard') == 'scissors decapitate lizard', 'Expected value is `scissors decapitate lizard`'
assert game('lizard', 'paper') == 'lizard eats paper', 'Expected value is `lizard eats paper`'
assert game('paper', 'spock') == 'paper disproves spock', 'Expected value is `paper disproves spock`'
assert game('spock', 'rock') == 'spock vaporizes rock', 'Expected value is `spock vaporizes rock`'
assert game('rock', 'scissors') == 'rock crushes scissors', 'Expected value is `rock crushes scissors`'