class PayrollSystem:

    def calculate_payroll(self, employees):
        for employee in employees:
            print(f'Payroll for: {employee.name}')
            print(f'Check amount: {employee.calculate_payroll()}')