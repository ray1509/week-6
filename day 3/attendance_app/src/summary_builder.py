from datetime import datetime

FORMAT = '%m/%d/%y, %I:%M:%S %p'


class Duration:
    def __init__(self, hours, minutes, seconds):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __str__(self):
        return {
            'hours': self.hours,
            'minutes': self.minutes,
            'seconds': self.seconds,
        }


class Summary:
    def __init__(self, title, id, attended_participants, start_time, end_time, duration: Duration):
        self.title = title
        self.id = id
        self.attended_participants = attended_participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration


def get_duration(start_time, end_time):

    start = datetime.strptime(start_time, FORMAT)
    end = datetime.strptime(end_time, FORMAT)
    seconds = int((end-start).total_seconds())
    minutes = int(((end-start).total_seconds())/60)
    hours = int(((end-start).total_seconds())/3600)
    while minutes >= 60:
        minutes = minutes - 60
    while seconds >= 60:
        seconds = seconds - 60
    return {
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    }


def normalize_summary(raw_data: dict):

    if not raw_data.get('Meeting Id'):

        title = raw_data.get('Meeting title')
        id = raw_data.get('Meeting title')
        attended_participants = raw_data.get('Attended participants')
        start_time = raw_data.get('Start time')
        end_time = raw_data.get('End time')
        duration = get_duration(
            str(raw_data.get('Start time')), str(raw_data.get('End time')))

        response = {
            'Title': title,
            'Id': id,
            'Attended participants': int(attended_participants),
            'Start Time': start_time,
            'End Time': end_time,
            'Duration': {
                'hours': duration.get('hours'),
                'minutes': duration.get('minutes'),
                'seconds': duration.get('seconds'),
            }
        }
        return response

    title = raw_data.get('Meeting Title')
    id = raw_data.get('Meeting Id')
    attended_participants = raw_data.get('Total Number of Participants')
    start_time = raw_data.get('Meeting Start Time')
    end_time = raw_data.get('Meeting End Time')
    duration = get_duration(str(raw_data.get('Meeting Start Time')), str(
        raw_data.get('Meeting End Time')))

    response = {
        'Title': title,
        'Id': id,
        'Attended participants': int(attended_participants),
        'Start Time': start_time,
        'End Time': end_time,
        'Duration': {
            'hours': duration.get('hours'),
            'minutes': duration.get('minutes'),
            'seconds': duration.get('seconds'),
        }
    }
    return response


def build_summary_object(raw_data: dict):

    duration = Duration(raw_data['Duration']['hours'],
                        raw_data['Duration']['minutes'],
                        raw_data['Duration']['seconds']
                        )

    response = Summary(
        title=raw_data.get('Title'),
        id=raw_data.get('Id'),
        attended_participants=raw_data.get('Attended participants'),
        start_time=raw_data.get('Start Time'),
        end_time=raw_data.get('End Time'),
        duration=duration
    )
    return response
