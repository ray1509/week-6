from src.duration import Duration


class Participant:

    def __init__(self, full_name: str, duration: Duration, join_time: str, leave_time: str, email: str, role: str, id: str):
        self.full_name = full_name
        self.duration = duration
        self.join_time = join_time
        self.leave_time = leave_time
        self.email = email
        self.role = role
        self.id = id

    @property
    def name(self):
        full_name = self.full_name.split(' ')
        name = full_name[0]
        return name

    @property
    def middle_name(self):
        full_name = self.full_name.split(' ')
        middle_name = full_name[1] if len(full_name) >= 4 else None
        return middle_name

    @property
    def first_surname(self):
        full_name = self.full_name.split(' ')
        first_surname = full_name[-2] if len(full_name) >= 3 else full_name[1]
        return first_surname

    @property
    def second_surname(self):
        full_name = self.full_name.split(' ')
        second_surname = full_name[-1] if len(full_name) >=3 else None
        return second_surname