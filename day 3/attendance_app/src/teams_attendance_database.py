from teams_files_reader import load_data
from meeting import Meeting

class TeamsAttendanceDatabase:

    def __init__(self):
        self.__attendace = load_data() or []
    
    @property
    def meetings(self):
        return [self.__create_meeting(**raw_attendance) for raw_attendance in self.__attendace]

    

    def __create_meeting(self, meeting_id, meeting_title, start_time, end_time, meeting_duration, attended_participants, average_attendance, participans, in_meeting_activities):
        id = ''
        title = ''
        attendances = []
        return Meeting(id, title, attendances)
