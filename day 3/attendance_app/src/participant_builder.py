from src.participant import Participant
from src.duration import Duration


def get_duration(time):
    duration = time.split(' ')
    hours = 0
    minutes = 0
    seconds = 0
    for d in duration:
        if d.find('h') > -1:
            hours = int(d.strip('h'))
        if d.find('m') > -1:
            minutes = int(d.strip('m'))
        if d.find('s') > -1:
            seconds = int(d.strip('s'))
    return {
        'Hours': hours,
        'Minutes': minutes,
        'Seconds': seconds
    }


def add_duration(raw):
    time = 0
    for participant in raw:
        a = get_duration(participant.get('Duration'))
        hours = a.get('Hours') * 3600
        minutes = a.get('Minutes') * 60
        seconds = a.get('Seconds')
        time = time + hours + minutes + seconds
    response = convert_time(time)
    return response


def convert_time(seconds):
    hours = int(seconds / 60 / 60)
    seconds -= hours*60*60
    minutes = int(seconds/60)
    seconds -= minutes*60
    return f"{hours}h {minutes:02d}m {seconds:02d}s"


def normalize_participant(raw: dict):

    name = raw.get('Name') or raw.get('Full Name')
    join_time = raw.get('First Join Time') or raw.get('Join Time')
    leave_time = raw.get('Last Leave time') or raw.get('Leave Time')
    duration = get_duration(
        raw.get('Duration') or raw.get('In-meeting Duration'))
    email = raw.get('Email')
    role = raw.get('Role')
    id = raw.get('Email') or raw.get('Participant ID (UPN)')

    return {
        'Full Name': name,
        'Join time': join_time,
        'Leave time': leave_time,
        'In-meeting Duration': duration,
        'Email': email,
        'Role': role,
        'Participant ID (UPN)': id
    }


def resolve_participant_join_last_time(raw: dict):
    name = raw[0].get('Full Name')
    join_time = raw[0].get('Join Time')
    leave_time = raw[-1].get('Leave Time')
    duration = add_duration(raw)
    email = raw[0].get('Email')
    role = raw[0].get('Role')
    id = raw[0].get('Participant ID (UPN)')

    return {
        'Name': name,
        'First Join Time': join_time,
        'Last Leave time': leave_time,
        'In-meeting Duration': duration,
        'Email': email,
        'Role': role,
        'Participant ID (UPN)': id
    }


def build_participant_object(raw: dict):
    if not raw.get('Full Name') or raw.get('Join Time') or raw.get('Leave Time'):
        raise ValueError('Full Name, Join time, Leave time cannot be None')

    name = raw.get('Name') or raw.get('Full Name')
    join_time = raw.get('Join time')
    leave_time = raw.get('Last Leave time') or raw.get('Leave time')
    duration = Duration(*raw['In-meeting Duration'].values())
    email = raw.get('Email')
    role = raw.get('Role')
    id = raw.get('Email') or raw.get('Participant ID (UPN)')

    participant = Participant(
        name, duration, join_time, leave_time, email, role, id)
    return participant

