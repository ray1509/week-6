from src.summary_builder import Summary, get_duration, Duration
from src.attendance import Atendance
from src.participant import Participant


def get_participants(data):
    response = []
    for participant in data:
        full_name = participant.get('Full Name')
        duration = get_duration(str(participant.get('Join time')),
                                str(participant.get('Leave time')))
        join_time = participant.get('Join time')
        leave_time = participant.get('Leave time')
        email = participant.get('Email')
        role = participant.get('Role')
        id = participant.get('Participant ID (UPN)')
        new = Participant(full_name, duration, join_time,
                          leave_time, email, role, id)
        response.append(new)

    return response


def build_attendance_object(raw: dict):
    start_datetime = raw.get('Start Time')
    title = raw.get('Title')
    id = raw.get('Id')
    attended_participants = raw.get('Attended participants')
    start_time = raw.get('Start Time')
    end_time = raw.get('End Time')
    duration = Duration(raw['Duration']['hours'],
                        raw['Duration']['minutes'],
                        raw['Duration']['seconds']
                        )
    summary = Summary(title, id, attended_participants,
                      start_time, end_time, duration)
    participants = get_participants(raw.get('Participants'))
    attendance = Atendance(start_datetime, summary, participants)
    return attendance
