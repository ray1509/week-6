scores = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,
         "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,
         "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,
         "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,
         "x": 8, "z": 10}

class Scrabble:
    
    def __init__(self):
        self.scores = scores
        
    def get_score(self, word):
        word_lower = word.lower()
        score = 0
        for letter in word_lower:
            for key, value in scores.items():
                if letter == key:
                    score += value
        return score


game = Scrabble()
assert game is not None, "Expected non-null object"
assert type(game) is Scrabble, f"Expected type {Scrabble}"

assert type(game.scores) is dict, f"Expected dict type for Scrabble's scores"
assert len(game.scores) == 26, f"Expected dict len is 28"

assert game.scores['c'] == 3, f'Expected value of 3 for key `c`'

word = "HOlla"
expected_score = 8
score = game.get_score("HOlla")
assert score == 8, f"Expected score for word {word} is {expected_score}"
