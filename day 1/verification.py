password = 'hola'
verification_code = '1234'


def verify_password(word):
    return True if word == password else False


def verify_code(code):
    return True if code == verification_code else False


def verification():
    chances = 3
    while chances > 0:
        user_password = input('Please enter your password: ')
        if (verify_password(user_password) == False):
            chances = chances - 1
            print('Please try again')
        else:
            user_code = input('please enter your code: ')
            if(verify_code(user_code)):
                print('Congratulations your account is verify')
            else:
                chances = chances-1
                print('Please enter a value code')
    print('Your account is blocked, contact with support')


verification()
