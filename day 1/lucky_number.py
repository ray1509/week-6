def validate_number(number):
    if number <= 0:
        return 'You have bad luck'
    else:
        return lucky_number(number)

def split_number(number):
    digits = []
    while number > 10:
        digits.append(number % 10)
        number = int(number/10)
    digits.append(number)
    return digits

def lucky_number(number):
    digits = split_number(number)

    for digit in digits :
        if digit not in [4,7]:
            return 'You have bad luck'
    return 'Congratulations your number is lucky'

