
accounts = [0.12, 0.90, 0.89, 0.076, -0.0087, 0.78, 0.65]


def transformer(balances):
    return [round((balance * 100), 2) for balance in balances]


expected_percentages = [12.0, 90.0, 89.0, 7.6, -0.87, 78.0, 65.0]

percentage_accounts = transformer(accounts)

assert percentage_accounts == expected_percentages, expected_percentages


