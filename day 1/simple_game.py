options = ['rock', 'paper', 'scisssors','lizard', 'spock']

def validate_choice(choice):
    if choice not in options:
        return False
    else:
        return True

def simple_game():
    winner = False
    while winner is False:
        player_1 = input("Enter player 1 selection: ")
        if validate_choice(player_1) is False:
            print('Invalid value, the game was reseted')
        else:
            player_2 = input("Enter player 2 selection: ")
            if validate_choice(player_2) is False:
                print('Invalid value, the game was reseted')
            else: 
                define_winner(player_1, player_2)
                return winner == True

def define_winner(option_1, option_2):
    winner = 'Player_1 is the winner'
    if option_1 == option_2:
        print('Its a tie, try again')
    elif option_1 == 'rock' and option_2 in ['scisssors', 'lizard']:
        print(winner)
    elif option_1 == 'paper' and option_2 in ['rock', 'spock']:
        print(winner)
    elif option_1 == 'scisssors' and option_2 in ['lizard', 'paper']:
        print(winner)
    elif option_1 == 'lizard' and option_2 in ['spock', 'paper']:
        print(winner)
    elif option_1 == 'spock' and option_2 in ['scisssors', 'rock']:
        print(winner)
    else:
        print('Player_2 is the winner')



simple_game()